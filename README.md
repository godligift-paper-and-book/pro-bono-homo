#   Pro Bono Homo

<a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/4.0/"><img alt="Лицензия Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-nd/4.0/88x31.png" /></a><br />Это произведение доступно по <a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/4.0/">лицензии Creative Commons «Attribution-NonCommercial-NoDerivatives» («Атрибуция-Некоммерчески-БезПроизводных») 4.0 Всемирная</a>.

## О чем

Это репозиторий книги "Pro Bono Homo". Я долго искал формат между удобством написания и работы с большими текстами, кажется вариант foliant + git репозиторий на данном этапе наиболее предпочтителен

##  Build Instructions

With pip and stuff (requires Python 3.6+, Pandoc, and TeXLive):

```bash
$ pip install -r requirements.txt
# Site:
$ foliant make site
# PDF:
$ foliant make pdf
```
